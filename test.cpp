#include <iostream>
#include <fstream>
#include <unistd.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

Mat image = Mat(512,448,CV_8UC1);
uchar pixels[448][512];

int opCount = 0;

struct ProcessorFlags {
  bool z;
  bool s;
  bool p;
  bool cy;
  bool ac;
};

struct InputFlags {
  bool coin;
  bool player1;
  bool fire;
  bool left;
  bool right;
};

class ProcessorState {
public:

  uint8_t a, b, c, d, e, h, l;
  uint16_t pc, sp;
  uint16_t cycles;
  uint16_t memory[16384];
  ProcessorFlags flags;
  InputFlags input;

  bool int_enable;
  bool interruptMode;
  uint8_t lastInterrupt;

  ProcessorState() {
    a = 0; b = 0; c = 0; d = 0; e = 0; h = 0; l = 0; sp = 0; pc = 0;
    flags.z = false; flags.s = false; flags.p = false; flags.cy = false; flags.ac = false;
    input.coin = false; input.player1 = false; input.fire = false; input.left = false; input.right = false;
    int_enable = false; interruptMode = false; lastInterrupt = 8;
  }

};

const int cyclesByOpcode[256] = {
  4, 10, 7, 6, 5, 5, 7, 7, 4, 11, 7, 6, 5, 5, 7, 4, //0x00..0x0f
	4, 10, 7, 6, 5, 5, 7, 7, 4, 11, 7, 6, 5, 5, 7, 4, //0x10..0x1f
	4, 10, 16, 6, 5, 5, 7, 4, 4, 11, 16, 6, 5, 5, 7, 4, //etc
	4, 10, 13, 6, 10, 10, 10, 4, 4, 11, 13, 6, 5, 5, 7, 4,

	5, 5, 5, 5, 5, 5, 7, 5, 5, 5, 5, 5, 5, 5, 7, 5, //0x40..0x4f
	5, 5, 5, 5, 5, 5, 7, 5, 5, 5, 5, 5, 5, 5, 7, 5,
	5, 5, 5, 5, 5, 5, 7, 5, 5, 5, 5, 5, 5, 5, 7, 5,
	7, 7, 7, 7, 7, 7, 7, 7, 5, 5, 5, 5, 5, 5, 7, 5,

	4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4, //0x80..8x4f
	4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4,
	4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4,
	4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4,

	11, 10, 10, 10, 18, 11, 7, 11, 11, 10, 10, 10, 11, 17, 7, 11, //0xc0..0xcf
  11, 10, 10, 10, 18, 11, 7, 11, 11, 10, 10, 10, 11, 17, 7, 11, //0xd0..0xdf
	11, 10, 10, 4, 18, 11, 7, 11, 11, 4, 10, 4, 18, 0, 7, 11,
  11, 10, 10, 4, 18, 11, 7, 11, 11, 6, 10, 4, 18, 0, 7, 11
};

void setZSPflags(uint8_t value, ProcessorFlags *flags) {
  flags->z = value == 0;
  flags->s = value > 127;
  flags->p = value % 2 == 0;
}

void updateInputState(uchar key, InputFlags *input) {
  input->coin = false; input->player1 = false; input->fire = false;
  input->left = false; input->right = false;
  switch ((int)key) {
    case 99: input->coin = true; break;
    case 49: input->player1 = true; break;
    case 32: input->fire = true; break;
    case 2: input->left = true; break;
    case 3: input->right = true; break;
  }
}

uint8_t convertInputToByte(InputFlags *input) {
  uint8_t byte = 0;
  if (input->coin) { byte += 1; }
  if (input->player1) { byte += 4; }
  if (input->fire) { byte += 16; }
  if (input->left) { byte += 32; }
  if (input->right) { byte += 64; }
  return byte;
}

uint8_t convertFlagsToByte(ProcessorFlags *flags) {
  uint8_t byte = 0;
  if (flags->z) byte += 128;
  if (flags->s) byte += 64;
  if (flags->p) byte += 32;
  if (flags->cy) byte += 16;
  return byte;
}

void restoreFlagsFromByte(uint8_t byte, ProcessorFlags *flags) {
  if (byte > 127) { flags->z = true; byte -= 128; }
  else { flags->z = false; }
  if (byte > 63) { flags->s = true; byte -= 64; }
  else { flags->s = false; }
  if (byte > 31) { flags->p = true; byte -= 32; }
  else { flags->p = false; }
  if (byte > 15) { flags->cy = true; }
  else { flags->cy = false; }
}

pair<int,int> seperateBytes(uint16_t word) {
  return pair<int,int> { (word >> 8), (word & 0xFF) };
}

bool unimplementedOpcode(int opcode) {
  cout << "Error Unimplemented Opcode: " << std::hex << opcode << endl;
  return false;
}

bool emulate(ProcessorState *state) {
  int opcode = state->memory[state->pc];
  int arg1 = state->memory[state->pc + 1];
  int arg2 = state->memory[state->pc + 2];

  state->pc++;

  uint8_t a, b, c, d, e, h, l;
  uint16_t bc, de, hl;
  uint8_t value;
  uint16_t addr, value16;
  pair<int,int> bytePair;

  switch (opcode) {
    case 0x00: break;
    case 0x01:
      state->c = arg1; state->b = arg2; state->pc += 2; break;
    case 0x03:
      bc = (state->b * 256) + state->c; bc += 1; bytePair = seperateBytes(bc);
      state->b = bytePair.first; state->c = bytePair.second; break;
    case 0x04:
      state->b++; setZSPflags(state->b, &state->flags); break;
    case 0x05:
      state->b--; setZSPflags(state->b, &state->flags); break;
    case 0x06:
      state->b = arg1; state->pc++; break;
    case 0x07: //RLC
      a = state->a * 2; if (state->a > 127) a++; state->flags.cy = (state->a > 127); state->a = a; break;
    case 0x09:
      hl = (state->h * 256) + state->l; bc = (state->b * 256) + state->c;
      hl += bc; state->flags.cy = (hl+bc) > 65535; bytePair = seperateBytes(hl);
      state->h = bytePair.first; state->l = bytePair.second; break;
    case 0x0a:
      addr = (state->b * 256) + state->c; state->a = state->memory[addr]; break;
    case 0x0c:
      state->c++; setZSPflags(state->c, &state->flags); break;
    case 0x0d:
      state->c--; setZSPflags(state->c, &state->flags); break;
    case 0x0e:
      state->c = arg1; state->pc++; break;
    case 0x0f:
      value = state->a; value = floor(value / 2); if (state->a % 2 != 0) value += 128;
      state->flags.cy = (state->a % 2 != 0); state->a = value; break;
    case 0x11:
      state->e = arg1; state->d = arg2; state->pc += 2; break;
    case 0x12:
      addr = (state->d * 256) + state->e; state->memory[addr] = state->a; break;
    case 0x13:
      state->e++; if (state->e == 0) { state->d++; } break;
    case 0x14:
      state->d++; setZSPflags(state->d, &state->flags); break;
    case 0x15:
      state->d--; setZSPflags(state->d, &state->flags); break;
    case 0x16:
      state->d = arg1; state->pc++; break;
    case 0x19:
      hl = (state->h * 256) + state->l; de = (state->d * 256) + state->e;
      hl += de; state->flags.cy = (hl+de) > 65535; bytePair = seperateBytes(hl);
      state->h = bytePair.first; state->l = bytePair.second; break;
    case 0x1a:
      addr = (state->d * 256) + state->e; state->a = state->memory[addr]; break;
    case 0x1b:
      addr = (state->d * 256) + state->e; addr--;
      bytePair = seperateBytes(addr); state->d = bytePair.first; state->e = bytePair.second; break;
    case 0x1f:
      value = state->a; value = floor(value / 2); if (state->flags.cy) value += 128;
      state->flags.cy = (state->a % 2 != 0); state->a = value; break;
    case 0x21:
      state->h = arg2; state->l = arg1; state->pc += 2; break;
    case 0x22:
      addr = (arg2 * 256) + arg1; state->memory[addr] = state->l; state->memory[addr+1] = state->h;
      state->pc += 2; break;
    case 0x23:
      state->l++; if (state->l == 0) { state->h++; } break;
    case 0x26:
      state->h = arg1; state->pc++; break;
    case 0x27: break; //Special
    case 0x29:
      hl = (state->h * 256) + state->l; hl += hl; state->flags.cy = (hl+hl) > 65535;
      bytePair = seperateBytes(hl); state->h = bytePair.first; state->l = bytePair.second; break;
    case 0x2a:
      addr = (arg2 * 256) + arg1; state->l = state->memory[addr]; state->h = state->memory[addr + 1]; state->pc += 2; break;
    case 0x2b:
      hl = (state->h * 256) + state->l; hl--; bytePair = seperateBytes(hl);
      state->h = bytePair.first; state->l = bytePair.second; break;
    case 0x2c:
      state->l++; setZSPflags(state->l, &state->flags); break;
    case 0x2e:
      state->l = arg1; state->pc++; break;
    case 0x2f:
      state->a = ~state->a; break;
    case 0x31:
      state->sp = (arg2 * 256) + arg1; state->pc += 2; break;
    case 0x32:
      addr = (arg2 * 256) + arg1; state->memory[addr] = state->a; state->pc += 2; break;
    case 0x34:
      addr = (state->h * 256) + state->l; value = state->memory[addr]; value += 1;
      setZSPflags(value, &state->flags); state->memory[addr] = value; break;
    case 0x35:
      addr = (state->h * 256) + state->l; value = state->memory[addr]; value -= 1;
      setZSPflags(value, &state->flags); state->memory[addr] = value; break;
    case 0x36:
      addr = (state->h * 256) + state->l;
      state->memory[addr] = arg1; state->pc++; break;
    case 0x37:
      state->flags.cy = true; break;
    case 0x3a:
      addr = (arg2 * 256) + arg1; state->a = state->memory[addr]; state->pc += 2; break;
    case 0x3c:
      state->a++; setZSPflags(state->a, &state->flags); break;
    case 0x3d:
      state->a--; setZSPflags(state->a, &state->flags); break;
    case 0x3e:
      state->a = arg1; state->pc++; break;
    case 0x41:
      state->b = state->c; break;
    case 0x46:
      addr = (state->h * 256) + state->l; state->b = state->memory[addr]; break;
    case 0x47:
      state->b = state->a; break;
    case 0x48:
      state->c = state->b; break;
    case 0x4e:
      addr = (state->h * 256) + state->l; state->c = state->memory[addr]; break;
    case 0x4f:
      state->c = state->a; break;
    case 0x56:
      addr = (state->h * 256) + state->l; state->d = state->memory[addr]; break;
    case 0x57:
      state->d = state->a; break;
    case 0x5e:
      addr = (state->h * 256) + state->l; state->e = state->memory[addr]; break;
    case 0x5f:
      state->e = state->a; break;
    case 0x61:
      state->h = state->c; break;
    case 0x65:
      state->h = state->l; break;
    case 0x66:
      addr = (state->h * 256) + state->l; state->h = state->memory[addr]; break;
    case 0x67:
      state->h = state->a; break;
    case 0x68:
      state->l = state->b; break;
    case 0x69:
      state->l = state->c; break;
    case 0x6f:
      state->l = state->a; break;
    case 0x70:
      addr = (state->h * 256) + state->l; state->memory[addr] = state->b; break;
    case 0x78:
      state->a = state->b; break;
    case 0x79:
      state->a = state->c; break;
    case 0x7a:
      state->a = state->d; break;
    case 0x7b:
      state->a = state->e; break;
    case 0x7c:
      state->a = state->h; break;
    case 0x7d:
      state->a = state->l; break;
    case 0x7e:
      addr = (state->h * 256) + state->l; state->a = state->memory[addr]; break;
    case 0x71:
      addr = (state->h * 256) + state->l; state->memory[addr] = state->c; break;
    case 0x77:
      addr = (state->h * 256) + state->l; state->memory[addr] = state->a; break;
    case 0x80:
      state->flags.cy = ((state->a + state->b) > 255);
      state->a += state->b; setZSPflags(state->a, &state->flags); break;
    case 0x81:
      state->flags.cy = ((state->a + state->c) > 255);
      state->a += state->c; setZSPflags(state->a, &state->flags); break;
    case 0x83:
      state->flags.cy = ((state->a + state->e) > 255);
      state->a += state->e; setZSPflags(state->a, &state->flags); break;
    case 0x85:
      state->flags.cy = ((state->a + state->l) > 255); state->a += state->l;
      setZSPflags(state->a, &state->flags); break;
    case 0x86:
      value = state->a; hl = (state->h * 256) + state->l; state->a += state->memory[hl];
      setZSPflags(state->a, &state->flags);
      state->flags.cy = ((state->memory[hl] + value) > 255); break;
    case 0x8a:
      value = state->a + state->d; state->a += state->d;
      if (state->flags.cy) { state->a++; value++; } setZSPflags(state->a, &state->flags);
      state->flags.cy = (value > 255); break;
    case 0x97:
      state->a = 0; setZSPflags(0, &state->flags); state->flags.cy = false; break;
    case 0xa0:
      state->a = state->a & state->b; setZSPflags(state->a, &state->flags); state->flags.cy = false; break;
    case 0xa7:
      state->a = state->a & state->a; setZSPflags(state->a, &state->flags); state->flags.cy = false; break;
    case 0xa6:
      addr = (state->h * 256) + state->l; value = state->memory[addr]; state->a = state->a & value;
      setZSPflags(state->a, &state->flags); state->flags.cy = false; break;
    case 0xa8:
      state->a = state->a ^ state->b; setZSPflags(state->a, &state->flags); state->flags.cy = false; break;
    case 0xaf:
      state->a = 0; setZSPflags(0, &state->flags); state->flags.cy = false; break;
    case 0xb0:
      state->a = state->a | state->b; setZSPflags(state->a, &state->flags); state->flags.cy = false; break;
    case 0xb4:
      state->a = state->a | state->h; setZSPflags(state->a, &state->flags); state->flags.cy = false; break;
    case 0xb6:
      addr = (state->h * 256) + state->l; state->a = state->a | state->memory[addr];
      setZSPflags(state->a, &state->flags); state->flags.cy = false; break;
    case 0xb8:
      value = state->a - state->b; state->flags.cy = ((state->a - state->b) < 0); setZSPflags(value, &state->flags); break;
    case 0xbc:
      value = state->a - state->h; state->flags.cy = ((state->a - state->h) < 0); setZSPflags(value, &state->flags); break;
    case 0xbe:
      addr = (state->h * 256) + state->l; value = state->a - state->memory[addr];
      state->flags.cy = ((state->a - state->memory[addr]) < 0); setZSPflags(value, &state->flags); break;
    case 0xc0:
      if (!state->flags.z) { state->pc = (state->memory[state->sp + 1] * 256) + state->memory[state->sp]; state->sp += 2; } break;
    case 0xc1:
      state->b = state->memory[state->sp + 1]; state->c = state->memory[state->sp]; state->sp += 2; break;
    case 0xc2:
      !state->flags.z ? state->pc = (arg2 * 256) + arg1 : state->pc += 2; break;
    case 0xc3:
      state->pc = (arg2 * 256) + arg1; break;
    case 0xc4:
      if (!state->flags.z) {
        state->memory[state->sp - 1] = seperateBytes(state->pc+2).first;
        state->memory[state->sp - 2] = seperateBytes(state->pc+2).second;
        state->sp -= 2; state->pc = (arg2 * 256) + arg1;
      } else { state->pc += 2; }
      break;
    case 0xc5:
      state->memory[state->sp - 2] = state->c; state->memory[state->sp - 1] = state->b; state->sp -= 2; break;
    case 0xc6:
      state->a += arg1; setZSPflags(state->a, &state->flags);
      state->flags.cy = ((state->a + arg1) > 255); state->pc++; break;
    case 0xc8:
      if (state->flags.z) { state->pc = (state->memory[state->sp + 1] * 256) + state->memory[state->sp]; state->sp += 2; }
      break;
    case 0xc9:
      state->pc = (state->memory[state->sp + 1] * 256) + state->memory[state->sp]; state->sp += 2; break;
    case 0xca:
      if (state->flags.z) { addr = (arg2 * 256) + arg1; state->pc = addr; break; }
      else { state->pc += 2; break; }
    case 0xcc:
      if (state->flags.z) {
        state->memory[state->sp - 1] = seperateBytes(state->pc+2).first;
        state->memory[state->sp - 2] = seperateBytes(state->pc+2).second;
        state->sp -= 2; state->pc = (arg2 * 256) + arg1;
      } else { state->pc += 2; } break;
    case 0xcd: //Call
      state->memory[state->sp - 1] = seperateBytes(state->pc+2).first;
      state->memory[state->sp - 2] = seperateBytes(state->pc+2).second;
      state->sp -= 2; state->pc = (arg2 * 256) + arg1; break;
    case 0xd0:
      if (!state->flags.cy) { state->pc = (state->memory[state->sp + 1] * 256) + state->memory[state->sp]; state->sp += 2; } break;
    case 0xd1:
      state->d = state->memory[state->sp + 1]; state->e = state->memory[state->sp]; state->sp += 2; break;
    case 0xd2:
      if (!state->flags.cy) { addr = (arg2 * 256) + arg1; state->pc = addr; break; }
      else { state->pc += 2; break; }
    case 0xd3: //Special
      state->pc++; break;
    case 0xd4:
      if (!state->flags.cy) {
        state->memory[state->sp - 1] = seperateBytes(state->pc+2).first;
        state->memory[state->sp - 2] = seperateBytes(state->pc+2).second;
        state->sp -= 2; state->pc = (arg2 * 256) + arg1; break;
      } else { state->pc += 2; } break;
    case 0xd5:
      state->memory[state->sp - 2] = state->e; state->memory[state->sp - 1] = state->d; state->sp -= 2; break;
    case 0xd6:
      state->a -= arg1; setZSPflags(state->a, &state->flags);
      state->flags.cy = (state->a == 255); state->pc++; break;
    case 0xd8:
      if (state->flags.cy) { state->pc = (state->memory[state->sp + 1] * 256) + state->memory[state->sp]; state->sp += 2; }
      break;
    case 0xda: //JC
      if (state->flags.cy) { addr = (arg2 * 256) + arg1; state->pc = addr; break; }
      else { state->pc += 2; break; }
    case 0xdb: //Special
      state->pc++; if(arg1 != 1) break; state->a = convertInputToByte(&state->input); break;
    case 0xde:
      value = arg1; if (state->flags.cy) value++; state->flags.cy = (value > state->a);
      state->a -= value; setZSPflags(state->a, &state->flags); state->pc++; break;
    case 0xe1:
      state->h = state->memory[state->sp + 1]; state->l = state->memory[state->sp]; state->sp += 2; break;
    case 0xe3:
      l = state->l; h = state->h; state->l = state->memory[state->sp]; state->h = state->memory[state->sp + 1];
      state->memory[state->sp] = l; state->memory[state->sp + 1] = h; break;
    case 0xe5:
      state->memory[state->sp - 2] = state->l; state->memory[state->sp - 1] = state->h; state->sp -= 2; break;
    case 0xe6:
      state->a = state->a & arg1; state->pc++; state->flags.cy = false; setZSPflags(state->a, &state->flags); break;
    case 0xe9:
      state->pc = (state->h * 256) + state->l; break;
    case 0xeb:
      d = state->d; e = state->e; state->d = state->h; state->e = state->l; state->h = d; state->l = e; break;
    case 0xf1:
      restoreFlagsFromByte(state->memory[state->sp], &state->flags);
      state->a = state->memory[state->sp + 1]; state->sp += 2; break;
    case 0xf5:
      state->memory[state->sp - 2] = convertFlagsToByte(&state->flags);
      state->memory[state->sp - 1] = state->a; state->sp -= 2; break;
    case 0xf6:
      state->flags.cy = false; state->a = state->a | arg1; setZSPflags(state->a, &state->flags); state->pc++; break;
    case 0xfa:
      addr = (arg2 * 256) + arg1; if (state->flags.s) state->pc = addr; else state->pc += 2; break;
    case 0xfb: //Special
      state->int_enable = true; break;
    case 0xfe:
      value = state->a - arg1; state->flags.cy = (state->a < arg1);
      setZSPflags(value, &state->flags); state->pc++; break;
    default: return unimplementedOpcode(opcode);
  }

  return true;
}

void interrupt(ProcessorState *state) {
  state->cycles -= 16667;
  if (!state->int_enable) return;
  state->interruptMode = true;
  state->lastInterrupt == 16 ? state->lastInterrupt = 8 : state->lastInterrupt = 16;
  state->memory[state->sp - 1] = seperateBytes(state->pc).first;
  state->memory[state->sp - 2] = seperateBytes(state->pc).second;
  state->sp -= 2; state->pc = state->lastInterrupt;
}

//7168

void updateDisplay(int videoPointer, uint16_t *memory) {
  for (int w = 0; w < 448; w+=2) {
    for (int h = 0; h < 512; h+=16) {
      int byte = memory[videoPointer];
      videoPointer++;
      for (int pixel = 0; pixel < 8; pixel++) {
        int p = pixel * 2;
        bool bit = 0 != (byte & (1<<pixel));
        bit ? image.at<uchar>(511-h-p,w) = 255 : image.at<uchar>(511-h-p,w) = 1;
        bit ? image.at<uchar>(511-h-p-1,w) = 255 : image.at<uchar>(511-h-p-1,w) = 1;
        bit ? image.at<uchar>(511-h-p,w+1) = 255 : image.at<uchar>(511-h-p,w+1) = 1;
        bit ? image.at<uchar>(511-h-p-1,w+1) = 255 : image.at<uchar>(511-h-p-1,w+1) = 1;
      }
    }
  }

}

int main() {

  //resize(image, image, Size(), 2, 2, CV_INTER_LINEAR);
  namedWindow("Display window", WINDOW_NORMAL);

  ProcessorState state = ProcessorState();

  fstream romData("invaders.rom", ios::in | ios::binary);
  for (int i = 0; i < 16384; i++) {
    int byte = romData.get();
    if (byte == -1) state.memory[i] = 255;
    else state.memory[i] = byte;
  }

  uint8_t vram[7168];

  while(emulate(&state)) {
    state.cycles += cyclesByOpcode[state.memory[state.pc]];
    if (state.cycles >= 16667) interrupt(&state);
    if (!state.interruptMode) opCount++;
    if (state.pc == 135) state.interruptMode = false;
    if (opCount % 10000 == 0) {  // render pixels
      updateDisplay(9216, state.memory);
      imshow("Display window", image);
      updateInputState(waitKey(1), &state.input);
    }
    //cout << std::dec << "OPCount: " << opCount << " state pc: " << std::hex << state.pc << " opcode: " << std::hex << state.memory[state.pc] << endl;
  }

  return 0;
}